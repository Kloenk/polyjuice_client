# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Message do
  @moduledoc """
  Functions for creating messages.

  These functions return a full event content, and do not fit in the
  `Polyjuice.Client.MsgBuilder` interface.  However, they make take a
  `Polyjuice.Client.MsgBuilder` where appropriate.

  """

  import Polyjuice.Client.MsgBuilder, only: [html_escape: 2]

  defp get_formatted_body(%{"formatted_body" => formatted_body}) when is_binary(formatted_body) do
    formatted_body
  end

  defp get_formatted_body(%{"body" => body}) when is_binary(body) do
    html_escape(body, false)
    |> IO.iodata_to_binary()
    |> String.replace("\n", "<br />\n")
  end

  defp to_message(msg, msgtype) do
    cond do
      Polyjuice.Client.MsgBuilder.MsgData.impl_for(msg) != nil ->
        Polyjuice.Client.MsgBuilder.to_message(msg, msgtype)

      is_map(msg) and not Map.has_key?(msg, :__struct__) ->
        msg

      true ->
        raise ArgumentError, message: "invalid argument msg"
    end
  end

  @doc ~S"""
  Create a reply.

  `strip_reply_fn` should be a function that takes an HTML-formatted message
  (as a string) and removes any `<mx-reply>` elements from it.  The input is
  untrusted, so the function should be able to handle malformed HTML.

  Example:

      iex> Polyjuice.Client.Message.reply(
      ...>   "Hello",
      ...>   %{
      ...>     "content" => %{"body" => "Hello World!", "msgtype" => "m.text"},
      ...>     "sender" => "@alice:example.org",
      ...>     "event_id" => "$event_id",
      ...>     "room_id" => "!room_id",
      ...>     "type" => "m.room.message"
      ...>   },
      ...>   # Note: We can only use the following as `strip_reply_fn` because
      ...>   # we know that the original message does not have <mx-reply> tags.
      ...>   # In real usage, this should be replaced with something better.
      ...>   fn msg -> msg end
      ...> )
      %{
        "body" => "> <@alice:example.org> Hello World!\nHello",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<mx-reply><blockquote>\n<a href=\"https://matrix.to/#/%21room_id/%24event_id\">In reply to</a> <a href=\"https://matrix.to/#/%40alice%3Aexample.org\">@alice:example.org</a><br />\nHello World!\n</blockquote></mx-reply>\nHello\n",
        "msgtype" => "m.text",
        "m.relates_to" => %{
          "m.in_reply_to" => %{
            "event_id" => "$event_id"
          }
        }
      }

  """
  @spec reply(
          msg :: Polyjuice.Util.event_content() | Polyjuice.Client.MsgBuilder.MsgData.t(),
          msgtype :: String.t(),
          ref_event :: Polyjuice.Util.event(),
          strip_reply_fn :: (String.t() -> String.t())
        ) :: Polyjuice.Util.event_content()
  def reply(
        msg,
        msgtype \\ "m.text",
        %{"content" => content, "sender" => sender, "event_id" => event_id, "room_id" => room_id},
        strip_reply_fn
      )
      when is_binary(msgtype) and is_map(content) and is_binary(sender) and is_binary(event_id) and
             is_binary(room_id) and is_function(strip_reply_fn, 1) do
    msg = to_message(msg, msgtype)

    stripped_reply =
      Map.get(content, "body", "")
      |> String.split("\n")
      |> Enum.drop_while(&String.starts_with?(&1, "> "))
      |> Enum.join("\n> ")

    formatted_body = get_formatted_body(msg)

    stripped_formatted_reply =
      case content do
        %{"formatted_body" => formatted_body} -> strip_reply_fn.(formatted_body)
        %{"body" => _body} -> get_formatted_body(content) |> strip_reply_fn.()
      end

    event_link =
      %Polyjuice.Util.URI{
        type: :event,
        identifier: {room_id, event_id}
      }
      |> to_string()
      |> html_escape(true)

    sender_link =
      %Polyjuice.Util.URI{
        type: :user,
        identifier: sender
      }
      |> to_string()
      |> html_escape(true)

    %{
      "msgtype" => msg["msgtype"],
      "body" => "> <#{sender}> #{stripped_reply}\n#{msg["body"]}",
      "formatted_body" => ~s"""
      <mx-reply><blockquote>
      <a href="#{event_link}">In reply to</a> <a href="#{sender_link}">#{
        html_escape(sender, false)
      }</a><br />
      #{stripped_formatted_reply}
      </blockquote></mx-reply>
      #{formatted_body}
      """,
      "format" => "org.matrix.custom.html",
      "m.relates_to" => %{
        "m.in_reply_to" => %{
          "event_id" => event_id
        }
      }
    }
  end

  # event type: m.reaction
  @doc ~S"""
  Create a reaction event.

  The event should be sent with type `m.reaction`.

  Example:

      iex> Polyjuice.Client.Message.react("👍", "$event_id")
      %{
        "m.relates_to" => %{
          "rel_type" => "m.annotation",
          "event_id" => "$event_id",
          "key" => "👍"
        }
      }
  """
  @spec react(reaction :: String.t(), ref_event :: Polyjuice.Util.event() | String.t()) ::
          Polyjuice.Util.event_content()
  def react(reaction, ref_event) when is_binary(reaction) and is_binary(ref_event) do
    %{
      "m.relates_to" => %{
        "rel_type" => "m.annotation",
        "event_id" => ref_event,
        "key" => reaction
      }
    }
  end

  def react(reaction, %{"event_id" => event_id})
      when is_binary(reaction) and is_binary(event_id) do
    react(reaction, event_id)
  end

  @doc ~S"""
  Create an event that edits a previous message.

  The new event contents is given in `msg`, and `fallback` is a fallback
  representation for clients that do not support edits.  `fallback` can be a
  function of one or two arguments, returning the fallback representation.  If
  it takes one argument, the argument will be the new event contents; if it
  takes two arguments, the first argument will be the new event contents and
  the second argument will be the old event contents.  If `fallback` is not
  given, then the function will generate a naive fallback.

  Note that this function assumes that the formatted body in `msg` is properly
  formatted.

  `ref_event` is the original event that is being edited.

  Example:

      iex> Polyjuice.Client.Message.edit(
      ...>   "Hello",
      ...>   %{
      ...>     "event_id" => "$event_id",
      ...>     "content" => %{"body" => "Helloo", "msgtype" => "m.text"}
      ...>   }
      ...> )
      %{
        "body" => "* Hello",
        "msgtype" => "m.text",
        "m.new_content" => %{
          "body" => "Hello",
          "msgtype" => "m.text",
        },
        "m.relates_to" => %{
          "rel_type" => "m.replace",
          "event_id" => "$event_id"
        }
      }

  """
  @spec edit(
          msg :: Polyjuice.Util.event_content() | Polyjuice.Client.MsgBuilder.MsgData.t(),
          msgtype :: String.t(),
          fallback ::
            Polyjuice.Util.event_content()
            | Polyjuice.Client.MsgBuilder.MsgData.t()
            | (Polyjuice.Util.event_content() -> Polyjuice.Util.event_content())
            | (Polyjuice.Util.event_content(), Polyjuice.Util.event_content() ->
                 Polyjuice.Util.event_content())
            | nil,
          ref_event :: Polyjuice.Util.event()
        ) :: Polyjuice.Util.event_content()
  def edit(
        msg,
        msgtype \\ "m.text",
        fallback \\ nil,
        %{
          "event_id" => event_id
        } = ref_event
      )
      when is_binary(event_id) do
    msg =
      case to_message(msg, msgtype) do
        %{
          "body" => body,
          "format" => "org.matrix.custom.html",
          "formatted_body" => formatted,
          "m.relates_to" => %{"m.in_reply_to" => %{"event_id" => _}}
        } = m ->
          # strip the fallback in replies, because clients that understand
          # edits are also expected to understand replies.
          stripped_body =
            String.split(body, "\n")
            |> Enum.drop_while(&String.starts_with?(&1, "> "))
            |> Enum.map(&"> #{&1}")
            |> Enum.join("\n")

          # Note: we assume that the formatted body is sane(-ish) HTML
          stripped_formatted_body =
            String.split(formatted, "</mx-reply>")
            |> List.last()

          %{m | "body" => stripped_body, "formatted_body" => stripped_formatted_body}

        m ->
          m
      end

    fallback =
      cond do
        fallback == nil ->
          case msg do
            %{
              "body" => body,
              "msgtype" => msgtype,
              "format" => format,
              "formatted_body" => formatted_body
            } ->
              # FIXME: just throwing a "*" in front of formatted HTML is kind of ugly,
              # but is probably the best we can do without pulling in an HTML parser.
              # FIXME: should we be starting with msg and then merging in the modified
              # keys, rather than creating a brand new map?
              %{
                "body" => "* #{body}",
                "msgtype" => msgtype,
                "format" => format,
                "formatted_body" => "* #{formatted_body}"
              }

            %{
              "body" => body,
              "msgtype" => msgtype
            } ->
              %{
                "body" => "* #{body}",
                "msgtype" => msgtype
              }

              # FIXME: anything else that we can have sensible(-ish) defaults for?
          end

        is_function(fallback, 1) ->
          fallback.(msg)

        is_function(fallback, 2) ->
          fallback.(msg, Map.get(ref_event, "content", %{}))

        true ->
          to_message(fallback, msgtype)
      end

    Map.merge(
      fallback,
      %{
        "m.new_content" => msg,
        "m.relates_to" => %{
          "rel_type" => "m.replace",
          "event_id" => event_id
        }
      }
    )
  end
end
