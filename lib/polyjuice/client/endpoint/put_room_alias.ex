# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomAlias do
  @moduledoc """
  Create a new mapping from room alias to room ID.
  https://matrix.org/docs/spec/client_server/latest#put-matrix-client-r0-directory-room-roomalias
  """

  @type t :: %__MODULE__{
          room_id: String.t(),
          room_alias: String.t()
        }

  @enforce_keys []

  defstruct [
    :room_id,
    :room_alias
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PutRoomAlias{
          room_id: room_id,
          room_alias: room_alias
        }) do
      body =
        %{room_id: room_id}
        |> Jason.encode!()

      Polyjuice.Client.Endpoint.HttpSpec.put(
        :r0,
        "directory/room/#{URI.encode_www_form(room_alias)}",
        body: body,
        auth_required: true
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, _parsed) do
      :ok
    end
  end
end
