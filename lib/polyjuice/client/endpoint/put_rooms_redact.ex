# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsRedact do
  @moduledoc """
  Send a redaction event to a room.

  https://matrix.org/docs/spec/client_server/r0.5.0#id308
  """

  @type t :: %__MODULE__{
          room: String.t(),
          txn_id: String.t(),
          event: String.t(),
          reason: String.t() | nil
        }

  @enforce_keys [:room, :txn_id, :event]
  defstruct [
    :room,
    :txn_id,
    :event,
    :reason
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PutRoomsRedact{
          room: room,
          event: event,
          txn_id: txn_id,
          reason: reason
        }) do
      e = &URI.encode_www_form/1

      body =
        case reason do
          reason when is_binary(reason) -> Jason.encode_to_iodata!(%{reason: reason})
          nil -> "{}"
        end

      Polyjuice.Client.Endpoint.HttpSpec.put(
        :r0,
        "rooms/#{e.(room)}/redact/#{e.(event)}/#{e.(txn_id)}",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "event_id")}
    end
  end
end
