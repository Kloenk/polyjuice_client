# Copyright 2020 Multi Prise <multiestunhappdev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetAccountData do
  @moduledoc """
  Set some account_data for the client.
  https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-user-userid-account-data-type
  """

  @type t :: %__MODULE__{
          user_id: String.t(),
          type: String.t()
        }

  @enforce_keys [:user_id, :type]

  defstruct [
    :user_id,
    :type
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.GetAccountData{
          user_id: user_id,
          type: type
        }) do
      e = &URI.encode_www_form/1

      Polyjuice.Client.Endpoint.HttpSpec.get(
        :r0,
        "user/#{e.(user_id)}/account_data/#{e.(type)}",
        auth_required: true
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end
end
