# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostRegister do
  @moduledoc """
  Register a user

  https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-register
  """

  @type t :: %__MODULE__{
          kind: :guest | :user,
          auth: %{type: String.t()} | nil,
          username: String.t() | nil,
          password: String.t() | nil,
          device_id: String.t() | nil,
          initial_device_display_name: String.t() | nil,
          inhibit_login: boolean()
        }

  defstruct [
    :kind,
    :auth,
    :username,
    :password,
    :device_id,
    :initial_device_display_name,
    inhibit_login: false
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PostRegister{
          kind: kind,
          auth: auth,
          username: username,
          password: password,
          device_id: device_id,
          initial_device_display_name: initial_device_display_name,
          inhibit_login: inhibit_login
        }) do
      body =
        [
          if(auth != nil, do: [{"auth", auth}], else: []),
          if(username != nil, do: [{"username", username}], else: []),
          if(password != nil, do: [{"password", password}], else: []),
          if(device_id != nil, do: [{"device_id", device_id}], else: []),
          if(inhibit_login, do: [{"inhibit_login", true}], else: []),
          if(initial_device_display_name != nil,
            do: [{"initial_device_display_name", initial_device_display_name}],
            else: []
          )
        ]
        |> Enum.concat()
        |> Map.new()
        |> Jason.encode_to_iodata!()

      query = [
        kind:
          if kind == nil do
            "user"
          else
            Atom.to_string(kind)
          end
      ]

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "register",
        body: body,
        query: query,
        auth_required: false
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end
end
