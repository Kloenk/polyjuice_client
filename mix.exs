defmodule PolyjuiceClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_client,
      description: "Client library for the Matrix.org communications protocol",
      version: "0.4.4",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),

      # Docs
      name: "Polyjuice Client",
      source_url: "https://gitlab.com/polyjuice/polyjuice_client",
      homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: [
          "README.md",
          "tutorial_echo.md",
          "tutorial_welcome.md"
        ],
        groups_for_modules: [
          Endpoints: [
            ~r/Polyjuice\.Client\.Endpoint\.Get.*/,
            ~r/Polyjuice\.Client\.Endpoint\.Post.*/,
            ~r/Polyjuice\.Client\.Endpoint\.Put.*/
          ]
        ]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/polyjuice/polyjuice_client"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: false]
      ]
    ]
  end

  def application do
    [
      mod: {Polyjuice.Client.Application, []},
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.12"},
      {:jason, "~> 1.2"},
      {:polyjuice_util, "~> 0.2.1"},
      {:mutex, "~> 1.1.3"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
