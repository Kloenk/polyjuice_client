# Polyjuice Client

Polyjuice Client is a [Matrix](https://matrix.org/) client library.  Also check
out [Polyjuice Util](https://hex.pm/packages/polyjuice_util/) for more functions.

## Installation

The package can be installed by adding `polyjuice_client` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:polyjuice_client, "~> 0.4.4"}
  ]
end
```

## Usage

To use this library, start by looking at the the
[tutorial](tutorial_echo.html), or the documentation for `Polyjuice.Client`.
