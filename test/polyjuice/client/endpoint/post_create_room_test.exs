# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostCreateRoomTest do
  use ExUnit.Case

  test "POST /_matrix/client/r0/createRoom with empty variables" do
    endpoint = %Polyjuice.Client.Endpoint.PostCreateRoom{}

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert %{http_spec | body: nil} == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: nil,
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"},
               {"Content-Type", "application/json"}
             ],
             method: :post,
             path: "_matrix/client/r0/createRoom"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({"room_id":"!room"})
           ) == {:ok, %{"room_id" => "!room"}}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end

  test "POST /_matrix/client/r0/createRoom with all variables" do
    endpoint = %Polyjuice.Client.Endpoint.PostCreateRoom{
      visibility: "public",
      room_alias_name: "#foo",
      name: "super fofo",
      topic: "about foo",
      invite: [],
      invite_3pid: [],
      room_version: "3",
      creation_content: %{
        "m.federate": false
      },
      initial_state: [],
      preset: :private_chat,
      is_direct: true,
      power_level_content_override: %{
        ban: 50,
        events: %{
          "m.room.name": 100,
          "m.room.power_levels": 100
        }
      }
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert %{http_spec | body: nil} == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: nil,
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"},
               {"Content-Type", "application/json"}
             ],
             method: :post,
             path: "_matrix/client/r0/createRoom"
           }

    assert Jason.decode!(http_spec.body) == %{
             "visibility" => "public",
             "room_alias_name" => "#foo",
             "name" => "super fofo",
             "topic" => "about foo",
             "room_version" => "3",
             "creation_content" => %{
               "m.federate" => false
             },
             "preset" => "private_chat",
             "is_direct" => true,
             "power_level_content_override" => %{
               "ban" => 50,
               "events" => %{
                 "m.room.name" => 100,
                 "m.room.power_levels" => 100
               }
             }
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({"room_id":"!room"})
           ) == {:ok, %{"room_id" => "!room"}}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
