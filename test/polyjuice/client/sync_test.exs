# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.SyncTest do
  use ExUnit.Case

  defmodule Httpd do
    def _matrix(session_id, env, input) do
      # FIXME: check method
      [path | _] =
        Keyword.get(env, :path_info)
        |> to_string()
        |> String.split("?", parts: 2)

      assert path == "client/r0/login" or
               Keyword.get(env, :http_authorization) == 'Bearer an_access_token'

      case path do
        "client/r0/user/%40alice%3Aexample.org/filter" ->
          handle_filter(session_id, env, input)

        "client/r0/sync" ->
          handle_sync(session_id, env, input)

        "client/r0/login" ->
          handle_login(session_id, env, input)

        "client/r0/logout" ->
          handle_logout(session_id, env, input)

        _ ->
          :mod_esi.deliver(
            session_id,
            'Status: 404 Not Found\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_NOT_FOUND","error":"Not found"}'
          )
      end
    end

    defp handle_filter(session_id, _env, _input) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: application/json\r\n\r\n{"filter_id":"1"}'
      )
    end

    defp handle_sync(session_id, env, _input) do
      sync =
        Keyword.get(env, :path_info)
        |> to_string()
        |> String.split("?", parts: 2)
        |> Enum.at(1)
        |> String.split("&")
        |> Enum.find("since=", &String.starts_with?(&1, "since="))
        |> String.replace_prefix("since=", "")

      response =
        case sync do
          "" ->
            %{
              "next_batch" => "1",
              "presence" => %{},
              "account_data" => %{},
              "rooms" => %{
                "invite" => %{
                  "!room_id" => %{
                    "invite_state" => %{
                      "events" => [
                        %{
                          "content" => %{
                            "membership" => "invite"
                          },
                          "type" => "m.room.member",
                          "state_key" => "@alice:example.org",
                          "event_id" => "$invite_event",
                          "room_id" => "!room_id",
                          "sender" => "@bob:example.org"
                        }
                      ]
                    }
                  }
                }
              }
            }

          "1" ->
            %{
              "next_batch" => "2",
              "presence" => %{},
              "account_data" => %{},
              "rooms" => %{
                "join" => %{
                  "!room_id" => %{
                    "state" => %{
                      "events" => [
                        %{
                          "content" => %{
                            "membership" => "join"
                          },
                          "type" => "m.room.member",
                          "state_key" => "@alice:example.org",
                          "event_id" => "$join_event",
                          "room_id" => "!room_id",
                          "sender" => "@alice:example.org"
                        }
                      ]
                    },
                    "timeline" => %{
                      "limited" => true,
                      "prev_batch" => "p1",
                      "events" => []
                    }
                  }
                }
              }
            }

          "2" ->
            %{
              "next_batch" => "3",
              "presence" => %{},
              "account_data" => %{},
              "rooms" => %{
                "join" => %{
                  "!room_id" => %{
                    "ephemeral" => %{
                      "events" => [
                        %{
                          "content" => %{"user_ids" => ["@alice:example.org"]},
                          "type" => "m.typing"
                        }
                      ]
                    }
                  }
                }
              }
            }

          "3" ->
            %{
              "next_batch" => "4",
              "presence" => %{},
              "account_data" => %{},
              "rooms" => %{
                "join" => %{
                  "!room_id" => %{
                    "timeline" => %{
                      "limited" => false,
                      "prev_batch" => "p2",
                      "events" => [
                        %{
                          "content" => %{
                            "msgtype" => "m.text",
                            "body" => "Hello World!"
                          },
                          "type" => "m.room.message",
                          "event_id" => "$message_event",
                          "room_id" => "!room_id",
                          "sender" => "@alice:example.org"
                        }
                      ]
                    }
                  }
                }
              }
            }

          "4" ->
            %{
              "next_batch" => "5",
              "presence" => %{},
              "account_data" => %{},
              "rooms" => %{
                "leave" => %{
                  "!room_id" => %{
                    "timeline" => %{
                      "limited" => false,
                      "prev_batch" => "p2",
                      "events" => [
                        %{
                          "content" => %{
                            "membership" => "leave",
                            "reason" => "Goodbye Cruel World!"
                          },
                          "type" => "m.room.member",
                          "state_key" => "@alice:example.org",
                          "event_id" => "$leave_event",
                          "room_id" => "!room_id",
                          "sender" => "@alice:example.org"
                        }
                      ]
                    }
                  }
                }
              }
            }

          _ ->
            nil
        end

      if response do
        :mod_esi.deliver(
          session_id,
          'Content-Type: application/json\r\n\r\n#{Jason.encode!(response)}'
        )
      end
    end

    defp handle_login(session_id, _env, _input) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: application/json\r\n\r\n{"user_id":"@alice:example.org","access_token":"an_access_token","device_id":"foo"}'
      )
    end

    defp handle_logout(session_id, _env, _input) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: application/json\r\n\r\n{}'
      )
    end
  end

  test "sync" do
    {:ok, tmpdir} = TestUtil.mktmpdir("sync-")

    storage = Polyjuice.Client.Storage.Ets.open()

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.Client.SyncTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      {:ok, client_pid} =
        Polyjuice.Client.start_link(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.SyncTest.Httpd",
          access_token: "an_access_token",
          user_id: "@alice:example.org",
          handler: self(),
          storage: storage,
          sync_filter: %{},
          test: true
        )

      client = Polyjuice.Client.get_client(client_pid)

      assert_receive({:polyjuice_client, :sync_connected})

      assert_receive(
        {:polyjuice_client, :invite,
         {"!room_id", "@bob:example.org",
          %{
            "m.room.member" => %{
              "@alice:example.org" => %{
                "content" => %{
                  "membership" => "invite"
                },
                "type" => "m.room.member",
                "state_key" => "@alice:example.org",
                "event_id" => "$invite_event",
                "room_id" => "!room_id",
                "sender" => "@bob:example.org"
              }
            }
          }}},
        1000
      )

      assert_receive({:polyjuice_client, :initial_sync_completed})

      assert_receive(
        {:polyjuice_client, :limited,
         {"!room_id", "p1",
          [
            %{
              "content" => %{"membership" => "join"},
              "event_id" => "$join_event",
              "room_id" => "!room_id",
              "sender" => "@alice:example.org",
              "state_key" => "@alice:example.org",
              "type" => "m.room.member"
            }
          ]}},
        1000
      )

      assert_receive(
        {:polyjuice_client, :state,
         {"!room_id",
          %{
            "content" => %{
              "membership" => "join"
            },
            "type" => "m.room.member",
            "state_key" => "@alice:example.org",
            "event_id" => "$join_event",
            "room_id" => "!room_id",
            "sender" => "@alice:example.org"
          }}},
        1000
      )

      assert_receive(
        {:polyjuice_client, :ephemeral,
         {"!room_id",
          %{"content" => %{"user_ids" => ["@alice:example.org"]}, "type" => "m.typing"}}},
        1000
      )

      assert_receive(
        {:polyjuice_client, :message,
         {"!room_id",
          %{
            "content" => %{
              "msgtype" => "m.text",
              "body" => "Hello World!"
            },
            "type" => "m.room.message",
            "event_id" => "$message_event",
            "room_id" => "!room_id",
            "sender" => "@alice:example.org"
          }}},
        1000
      )

      assert_receive(
        {:polyjuice_client, :state,
         {"!room_id",
          %{
            "content" => %{
              "membership" => "leave",
              "reason" => "Goodbye Cruel World!"
            },
            "type" => "m.room.member",
            "state_key" => "@alice:example.org",
            "event_id" => "$leave_event",
            "room_id" => "!room_id",
            "sender" => "@alice:example.org"
          }}},
        1000
      )

      assert_receive(
        {:polyjuice_client, :left, {"!room_id"}},
        1000
      )

      Polyjuice.Client.API.stop(client)

      :inets.stop(:httpd, httpd_pid)
    after
      Polyjuice.Client.Storage.close(storage)
      File.rm_rf(tmpdir)
    end
  end

  test "sync starts and stops on login/logout" do
    {:ok, tmpdir} = TestUtil.mktmpdir("sync-")

    storage = Polyjuice.Client.Storage.Ets.open()

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.Client.SyncTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      {:ok, client_pid} =
        Polyjuice.Client.start_link(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.SyncTest.Httpd",
          handler: self(),
          storage: storage,
          sync_filter: %{},
          test: true
        )

      client = Polyjuice.Client.get_client(client_pid)

      {:ok, _} = Polyjuice.Client.log_in_with_password(client, "@alice:example.org", "password")

      assert_receive({:polyjuice_client, :sync_connected})

      Polyjuice.Client.log_out(client)

      # make sure the sync has time to stop
      Process.sleep(10)

      assert Polyjuice.Client.process_name(client.id, :supervisor)
             |> Supervisor.which_children()
             |> Enum.find(fn {id, _, _, _} -> id == Polyjuice.Client.Sync end) == nil

      Polyjuice.Client.API.stop(client)

      :inets.stop(:httpd, httpd_pid)
    after
      Polyjuice.Client.Storage.close(storage)
      File.rm_rf(tmpdir)
    end
  end

  test "sync terminates when user logged out (with filter)" do
    {:ok, tmpdir} = TestUtil.mktmpdir("sync-")

    storage = Polyjuice.Client.Storage.Ets.open()

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd.LoggedOut]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      {:ok, client_pid} =
        Polyjuice.Client.start_link(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd.LoggedOut",
          access_token: "an_access_token",
          user_id: "@alice:example.org",
          handler: self(),
          storage: storage,
          sync_filter: %{},
          test: true
        )

      client = Polyjuice.Client.get_client(client_pid)

      assert_receive({:polyjuice_client, :logged_out, {false}}, 500)

      # make sure the sync has time to stop
      Process.sleep(10)

      assert Polyjuice.Client.process_name(client.id, :supervisor)
             |> DynamicSupervisor.which_children() == []

      Polyjuice.Client.API.stop(client)

      :inets.stop(:httpd, httpd_pid)
    after
      Polyjuice.Client.Storage.close(storage)
      File.rm_rf(tmpdir)
    end
  end

  test "sync terminates when user logged out (no filter)" do
    {:ok, tmpdir} = TestUtil.mktmpdir("sync-")

    storage = Polyjuice.Client.Storage.Ets.open()

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd.LoggedOut]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      {:ok, client_pid} =
        Polyjuice.Client.start_link(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd.LoggedOut",
          access_token: "an_access_token",
          user_id: "@alice:example.org",
          handler: self(),
          storage: storage,
          test: true
        )

      client = Polyjuice.Client.get_client(client_pid)

      assert_receive({:polyjuice_client, :logged_out, {false}})

      # make sure the sync has time to stop
      Process.sleep(10)

      assert Polyjuice.Client.process_name(client.id, :supervisor)
             |> DynamicSupervisor.which_children() == []

      Polyjuice.Client.API.stop(client)

      :inets.stop(:httpd, httpd_pid)
    after
      Polyjuice.Client.Storage.close(storage)
      File.rm_rf(tmpdir)
    end
  end
end
