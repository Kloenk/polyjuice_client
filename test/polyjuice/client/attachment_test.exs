# Copyright 2021 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.AttachmentTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Attachment

  test "new" do
    with client = %DummyClient{
           response:
             {%Polyjuice.Client.Endpoint.PostMediaUpload{
                data: {:file, "test.jpeg"},
                filename: "test.jpeg",
                mimetype: "image/jpeg"
              }, {:ok, "mxc://example.org/aabbccddee"}}
         } do
      msg =
        Polyjuice.Client.Attachment.new(
          client,
          {:file, "test.jpeg"},
          mimetype: "image/jpeg",
          size: 12
        )

      assert msg == %{
               "msgtype" => "m.file",
               "body" => "test.jpeg",
               "info" => %{"size" => 12, "mimetype" => "image/jpeg"},
               "url" => "mxc://example.org/aabbccddee"
             }
    end
  end

  test "detects file size" do
    with client = %DummyClient{
           response:
             {%Polyjuice.Client.Endpoint.PostMediaUpload{
                data: "aaaaaaaa",
                filename: "test.txt",
                mimetype: "text/plain"
              }, {:ok, "mxc://example.org/aabbccddee"}}
         } do
      msg =
        Polyjuice.Client.Attachment.new(
          client,
          {:data, "aaaaaaaa", "test.txt"},
          mimetype: "text/plain"
        )

      assert msg == %{
               "msgtype" => "m.file",
               "body" => "test.txt",
               "info" => %{"size" => 8, "mimetype" => "text/plain"},
               "url" => "mxc://example.org/aabbccddee"
             }
    end

    with client = %DummyClient{
           response:
             {%Polyjuice.Client.Endpoint.PostMediaUpload{
                data: {:file, "README.md"},
                filename: "README.md",
                mimetype: "text/plain"
              }, {:ok, "mxc://example.org/aabbccddee"}}
         } do
      msg =
        Polyjuice.Client.Attachment.new(
          client,
          {:file, "README.md"},
          mimetype: "text/plain"
        )

      assert is_integer(msg["info"]["size"])
    end
  end

  test "as_image" do
    msg = %{
      "msgtype" => "m.file",
      "body" => "test.jpeg",
      "info" => %{"size" => 12, "mimetype" => "image/jpeg"},
      "url" => "mxc://example.org/aabbccddee"
    }

    assert Polyjuice.Client.Attachment.as_image(msg, width: 200, height: 200) == %{
             "msgtype" => "m.image",
             "body" => "test.jpeg",
             "info" => %{"size" => 12, "h" => 200, "w" => 200, "mimetype" => "image/jpeg"},
             "url" => "mxc://example.org/aabbccddee"
           }
  end

  test "as_video" do
    msg = %{
      "msgtype" => "m.file",
      "body" => "test.mp4",
      "info" => %{"size" => 32444, "mimetype" => "video/mp4"},
      "url" => "mxc://example.org/aabbccddee"
    }

    assert Polyjuice.Client.Attachment.as_video(msg, duration: 421, width: 200, height: 200) == %{
             "msgtype" => "m.video",
             "body" => "test.mp4",
             "info" => %{
               "size" => 32444,
               "h" => 200,
               "w" => 200,
               "duration" => 421,
               "mimetype" => "video/mp4"
             },
             "url" => "mxc://example.org/aabbccddee"
           }
  end

  test "as_audio" do
    msg = %{
      "msgtype" => "m.file",
      "body" => "Bee Bee Gees - Stayin' Alive live",
      "info" => %{"size" => 1_563_685, "mimetype" => "audio/mpeg"},
      "url" => "mxc://example.org/aabbccddee"
    }

    assert Polyjuice.Client.Attachment.as_audio(msg, 231) == %{
             "msgtype" => "m.audio",
             "body" => "Bee Bee Gees - Stayin' Alive live",
             "info" => %{"size" => 1_563_685, "duration" => 231, "mimetype" => "audio/mpeg"},
             "url" => "mxc://example.org/aabbccddee"
           }
  end

  test "add_thumbnail" do
    with client = %DummyClient{
           response:
             {%Polyjuice.Client.Endpoint.PostMediaUpload{
                data: {:file, "test_thumbnail.jpeg"},
                filename: "test_thumbnail.jpeg",
                mimetype: "image/jpeg"
              }, {:ok, "mxc://example.org/thaabbccddee"}}
         } do
      msg =
        Polyjuice.Client.Attachment.new(
          client,
          {:url, "mxc://example.org/aabbccddee"},
          body: "test.jpeg",
          mimetype: "image/jpeg",
          size: 4500
        )
        |> Polyjuice.Client.Attachment.as_image(width: 200, height: 200)

      opts = [
        width: 50,
        height: 50,
        size: 300
      ]

      assert Polyjuice.Client.Attachment.add_thumbnail(
               msg,
               client,
               {:file, "test_thumbnail.jpeg"},
               "image/jpeg",
               opts
             ) == %{
               "msgtype" => "m.image",
               "body" => "test.jpeg",
               "info" => %{
                 "size" => 4500,
                 "mimetype" => "image/jpeg",
                 "h" => 200,
                 "w" => 200,
                 "thumbnail_info" => %{
                   "w" => 50,
                   "h" => 50,
                   "size" => 300,
                   "mimetype" => "image/jpeg"
                 },
                 "thumbnail_url" => "mxc://example.org/thaabbccddee"
               },
               "url" => "mxc://example.org/aabbccddee"
             }
    end
  end
end
