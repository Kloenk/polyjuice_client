# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule TestUtil do
  def mktmpdir(prefix) do
    Enum.find_value(
      1..5,
      {:error, :efail},
      fn _ ->
        random =
          Enum.map(
            Range.new(0, 12),
            fn _ -> Enum.random('abcdefghijklmnopqrstuvwxyz1234567890') end
          )
          |> to_string()

        tmpdir = Path.join(System.tmp_dir!(), prefix <> random)

        case File.mkdir(tmpdir) do
          :ok -> {:ok, tmpdir}
          # try again if the name is already taken
          {:error, :eexist} -> nil
          # pass through any other errors
          {:error, _} = err -> err
        end
      end
    )
  end

  def http_spec_body_to_binary(http_spec) do
    Map.update!(http_spec, :body, &IO.iodata_to_binary/1)
  end
end
